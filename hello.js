const helloContainer = document.querySelector(".js-hello"), 
    hello = helloContainer.querySelector(".hello");

const helloList =["ìëíì¸ì", "ë°ê°ìµëë¤", "Hello", "Hi", "íì´í"]

const HELLO_NUM = helloList.length;

function helloText(num) {
    hello.innerText = helloList[num];
}

function genRandom() {
    const number = Math.floor(Math.random() * HELLO_NUM);
    return number;
}

function init() {
    const randomNumber = genRandom();
    helloText(randomNumber);
    console.log(randomNumber);
}

init();